package com.barthelemot.kylian.projetboitedenuit;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.content.Intent;


import androidx.fragment.app.Fragment;

import com.barthelemot.kylian.projetboitedenuit.entity.Boite;
import com.barthelemot.kylian.projetboitedenuit.singleton.BoiteSingleton;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class BoiteFragment extends Fragment  implements OnMapReadyCallback {


    private Boite boite;

    private ImageView mCover;
    private TextView mAdresse;
    private TextView mPhoneNumber;
    private TextView mDescription;
    private TextView mAge;
    private TextView mHoraire;
    private TextView mOuvert;
    private Fragment map;

    private ImageButton btnPhone;
    private ImageButton btnGeo;
    private Button boutonmap;

    private Toolbar mToolbar;

    public BoiteFragment(Boite boite) {
        this.boite = boite;
    }

    private GoogleMap mMap;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return (ViewGroup) inflater.inflate(R.layout.fragment_boite, container);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mCover = (ImageView) view.findViewById(R.id.imageView_cover);
        mAdresse = (TextView) view.findViewById(R.id.tv_adresse);
        mPhoneNumber = (TextView) view.findViewById(R.id.tv_phoneNumber);
        mDescription = (TextView) view.findViewById(R.id.tv_description);
        mAge = (TextView) view.findViewById(R.id.tv_age);
        mHoraire = (TextView) view.findViewById(R.id.tv_horaire);
        mOuvert = (TextView) view.findViewById(R.id.tv_Ouverture);
        boutonmap = (Button) view.findViewById(R.id.boutonmap);


        btnGeo = (ImageButton) view.findViewById(R.id.imgBtn_map);
        btnGeo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openMap(v);
            }
        });


        boutonmap.setOnClickListener((new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToMapsActivity();
            }
        }));

        btnPhone = (ImageButton) view.findViewById(R.id.imgBtn_phone);
        btnPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialPhoneNumber(v);
            }
        });


        mToolbar = (Toolbar) view.findViewById(R.id.toolBar);
        mToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_white));
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        setBoite();

        boutonmap.setOnClickListener((new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToMapsActivity();
            }
        }));

    }

    private void goToMapsActivity() {
        BoiteSingleton.getInstance().getCurrentBoite().getNom();
        Intent intent = new Intent(this.getContext(), MapsActivity.class);
        startActivity(intent);
    }


    private void setBoite() {
        mToolbar.setTitle(boite.getNom());
        Picasso.get().load(boite.getUrlImage()).into(mCover);
        if (boite.isOuvert()) {
            mOuvert.setText("Ouvert");
            mOuvert.setTextColor(Color.rgb(50, 200, 80));
        } else {
            mOuvert.setText("Fermé");
            mOuvert.setTextColor(Color.RED);
        }
        Log.d("fragBoite", boite.getAgeMoyen() + boite.getNom());
        mAge.setText("Moyenne d'âge : " + boite.getAgeMoyen() + " ans");
        mDescription.setText(boite.getDescription());
        mDescription.setMovementMethod(new ScrollingMovementMethod());
        mPhoneNumber.setText(boite.getTel());
        mAdresse.setText(boite.getAdresse());
    }

    public void dialPhoneNumber(View v) {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + boite.getTel()));
        startActivity(intent);
    }

    public void openMap(View v) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        ArrayList<String> al = new ArrayList<String>();
        String adresse = boite.getAdresse();
        String tmp = "";
        for (int i = 0; i < adresse.length(); i++) {
            if (adresse.charAt(i) == ' ') {
                al.add(tmp);
                tmp = "";
            } else {
                tmp += adresse.charAt(i);
            }
        }
        int cpt = 0;
        String geo = "geo:0,0?q=";
        for (String s : al) {
            geo += s + "+";
        }
        intent.setData(Uri.parse(geo));
        startActivity(intent);
    }

    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        LatLng smile = new LatLng(50.6320292, 3.0553982);
        LatLng releve = new LatLng(50.6332982, 3.0549429);
        LatLng corner = new LatLng(50.6331117, 3.0551955);
        LatLng room = new LatLng(50.6284212, 3.0520797);
        //Le Smile
        //La Relève
        //O Corner
        //The Room
        mMap.addMarker(new MarkerOptions()
                .position(smile)
                .title("Localisation du Smile"));
        if (BoiteSingleton.getInstance().getBoite(getId()).getNom().equals("Le Smile")) {
            mMap.addMarker(new MarkerOptions()
                    .position(smile)
                    .title("Localisation du Smile"));
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(smile, 15));
        }

        if (BoiteSingleton.getInstance().getCurrentBoite().getNom().equals("La Relève")) {
            mMap.addMarker(new MarkerOptions()
                    .position(releve)
                    .title("Localisation de la Relève"));
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(releve, 15));
        }
        if (BoiteSingleton.getInstance().getCurrentBoite().getNom().equals("O Corner")) {
            mMap.addMarker(new MarkerOptions()
                    .position(corner)
                    .title("Localisation du Corner"));
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(corner, 15));
        }
        if (BoiteSingleton.getInstance().getCurrentBoite().getNom().equals("The Room")) {
            mMap.addMarker(new MarkerOptions()
                    .position(room)
                    .title("Localisation de The Room"));
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(room, 15));
        }
    }

}
package com.barthelemot.kylian.projetboitedenuit;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.SQLException;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.barthelemot.kylian.projetboitedenuit.dataBase.AppDbHelper;
import com.barthelemot.kylian.projetboitedenuit.dataBase.DBManager;
import com.barthelemot.kylian.projetboitedenuit.entity.Utilisateur;
import com.barthelemot.kylian.projetboitedenuit.singleton.UserSingleton;

import java.util.ArrayList;


public class LoginActivity extends AppCompatActivity {

    private DBManager mDBManager;

    private EditText mail;
    private EditText mdp;
    private Button connexion;
    private TextView inscription;
    private TextView messageError;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        SharedPreferences sharedPref = getSharedPreferences(getString(R.string.database_version), MODE_PRIVATE);

        mDBManager = new DBManager(this);
        try{
            mDBManager.open();
        } catch (SQLException e) {
            e.printStackTrace();
            Log.d("Database",e.getLocalizedMessage());
        }

        String currentDatabaseVersion = sharedPref.getString(getString(R.string.database_version), "");
        if(!currentDatabaseVersion.equals("0")){
            mDBManager.createDataBaseSchema();
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString(getString(R.string.database_version), String.valueOf(AppDbHelper.DATABASE_VERSION));
        }


        mail = (EditText) findViewById(R.id.editText_mail);
        mdp = (EditText) findViewById(R.id.editText_mdp);
        connexion = (Button) findViewById(R.id.connexion);
        inscription = (TextView) findViewById((R.id.btn_login));
        messageError = (TextView) findViewById((R.id.tv_errorLogin));
        inscription.getPaint().setUnderlineText(true);
        messageError.setVisibility(View.INVISIBLE);
        messageError.setTextColor(Color.RED);

        UserSingleton.getInstance().setUserList((ArrayList) mDBManager.getAllUtilisateurs());

        connexion.setOnClickListener(new View.OnClickListener() { //Au click sur le bouton de connection, lance la méthode connexion
            @Override
            public void onClick(View v) {
                connexion(v);
            }
        });

        inscription.setOnClickListener(new View.OnClickListener() { //Au click sur le texte "Pas encore de compte ?" envoie sur l'activité register
            @Override
            public void onClick(View v) {
                goToRegisterActivity();
            }
        });

        mdp.addTextChangedListener(new TextWatcher() { //A l'écriture dans les champs de texte, le message d'erreur disparait
            @Override
            public void afterTextChanged(Editable s) {}
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() != 0)
                    messageError.setVisibility(View.INVISIBLE);
            }
        });

        mail.addTextChangedListener(new TextWatcher() { //Idem pour le champ de mail
            @Override
            public void afterTextChanged(Editable s) {}
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() != 0)
                    messageError.setVisibility(View.INVISIBLE);
            }
        });
    }

    public void connexion (View v){ //Vérifie que le mail et le mdp correspondent à un compte utilisateur, si oui l'envoie à la page principale
        boolean check2=false;
        for (Utilisateur user : UserSingleton.getInstance().getUserList()) {
            if(mail.getText().toString().equals(user.getMail()) && mdp.getText().toString().equals(user.getMdp())){
                UserSingleton.getInstance().setCurrentUser(user);
                check2=true;
            }
        }
        if (check2==true) {
            Log.d("CurrentUserIs", UserSingleton.getInstance().getCurrentUser().getPrenom() + " " + UserSingleton.getInstance().getCurrentUser().isAdmin());
            goToMainActivity();
        } else {
            messageError.setVisibility(View.VISIBLE); // Sinon affiche un message d'erreur
        }

    }
    public void goToMainActivity() { // Méthode pour envoyer vers l'activité principale
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
    public void goToRegisterActivity() { // Méthode pour envoyer vers l'activité register
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onResume(){
        super.onResume();
        try{
            mDBManager.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mDBManager.close();
    }
}
package com.barthelemot.kylian.projetboitedenuit.dataBase;

import android.provider.BaseColumns;

public abstract class UtilisateurDbEntry implements BaseColumns {
    public static final String TABLE_NAME = "utilisateur";
    public static final String COLUMN_NAME_ID = "idUtilisateur";
    public static final String COLUMN_NAME_NOM = "nomUtilisateur";
    public static final String COLUMN_NAME_PRENOM = "prenomUtilisateur";
    public static final String COLUMN_NAME_MAIL = "mailUtilisateur";
    public static final String COLUMN_NAME_MDP = "mdpUtilisateur";
    public static final String COLUMN_NAME_ADMIN = "admin";
}

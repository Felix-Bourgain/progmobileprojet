package com.barthelemot.kylian.projetboitedenuit;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import android.content.ClipData;
import android.content.Intent;
import android.database.SQLException;
import android.os.Bundle;
import android.os.UserManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.barthelemot.kylian.projetboitedenuit.dataBase.DBManager;
import com.barthelemot.kylian.projetboitedenuit.dataBase.UtilisateurDbEntry;
import com.barthelemot.kylian.projetboitedenuit.entity.Utilisateur;
import com.barthelemot.kylian.projetboitedenuit.singleton.BoiteSingleton;
import com.barthelemot.kylian.projetboitedenuit.singleton.EventSingleton;
import com.barthelemot.kylian.projetboitedenuit.singleton.UserSingleton;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private DBManager mDBManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mDBManager = new DBManager(this);

        try{
            mDBManager.open();
        } catch (SQLException e) {
            e.printStackTrace();
            Log.d("Database",e.getLocalizedMessage());
        }

        BoiteSingleton.getInstance().setBoiteList((ArrayList) mDBManager.getAllBoite());
        EventSingleton.getInstance().setEventList((ArrayList) mDBManager.getAllEvenement());

        BottomNavigationView navView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_accueil, R.id.navigation_listeboites)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(navView, navController);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.top_nav_menu,menu);
        if(UserSingleton.getInstance().getCurrentUser().isAdmin()){
            menu.getItem(1).setVisible(true);
            menu.getItem(2).setVisible(true);
        }
        else{
            menu.getItem(1).setVisible(false);
            menu.getItem(2).setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch(id){
            case R.id.navigation_mesinfos:
                goToMesinfosActivity();
                break;
            case R.id.navigation_ajoutBoite:
                goToAjoutBoiteActivity();
                break;
            case R.id.navigation_ajoutEvent:
                goToAjoutEventActivity();
                break;
            case R.id.navigation_sedeconnecter:
                goToLoginActivity();
                UserSingleton.getInstance().setCurrentUser(null);
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    private void goToLoginActivity() {
        Intent intent = new Intent(this,LoginActivity.class);
        startActivity(intent);
    }

    public void goToMesinfosActivity() {
        Intent intent = new Intent(this,MesinfosActivity.class);
        startActivity(intent);
    }

    public void goToAjoutBoiteActivity(){
        Intent intent = new Intent(this, AjoutBoiteActivity.class);
        startActivity(intent);
    }

    public void goToAjoutEventActivity() {
        Intent intent = new Intent(this, AjoutEventActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onResume(){
        super.onResume();
        try{
            mDBManager.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mDBManager.close();
    }
}
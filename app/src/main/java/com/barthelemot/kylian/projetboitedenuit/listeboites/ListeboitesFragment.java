package com.barthelemot.kylian.projetboitedenuit.listeboites;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.barthelemot.kylian.projetboitedenuit.PagerBoiteActivity;
import com.barthelemot.kylian.projetboitedenuit.R;
import com.barthelemot.kylian.projetboitedenuit.entity.Boite;
import com.barthelemot.kylian.projetboitedenuit.singleton.BoiteSingleton;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

public class ListeboitesFragment extends Fragment {

    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_listeboites, container, false);

        recyclerView = root.findViewById(R.id.recyclerView_listBoite);
        layoutManager = new LinearLayoutManager(this.getContext());
        recyclerView.setLayoutManager(layoutManager);
        adapter = new BoiteAdapter();
        recyclerView.setAdapter(adapter);

        return root;
    }

    public void goToBoiteDetail(int id) {
        Intent intent = new Intent(this.getContext(), PagerBoiteActivity.class);
        intent.putExtra("id", id);

        startActivity(intent);
    }

    public class BoiteAdapter extends RecyclerView.Adapter<BoiteAdapter.ViewHolder> {

        public BoiteAdapter(){
            super();
        }

        @NonNull
        @Override
        public BoiteAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.boite_item_list, parent, false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(@NonNull BoiteAdapter.ViewHolder holder, int position) {
            if(position % 2 == 0) {
                holder.setBoite(BoiteSingleton.getInstance().getBoite(position), Color.rgb(2, 176, 159));
            }
            else {
                holder.setBoite(BoiteSingleton.getInstance().getBoite(position), Color.rgb(161,161,161));
            }
        }

        @Override
        public int getItemCount() {
            return BoiteSingleton.getInstance().getCountBoite();
        }


        public class ViewHolder extends RecyclerView.ViewHolder {
            private ImageView mLogo;
            private TextView mNom;
            private TextView mHoraire;

            public ViewHolder(View v) {
                super(v);
                mLogo = (ImageView) v.findViewById(R.id.imageView_logoBoite);
                mNom = (TextView) v.findViewById(R.id.tv_nomBoite);
                mHoraire = (TextView) v.findViewById(R.id.tv_horaire);
            }

            public void setBoite(final Boite boite, int lineColor) {
                this.itemView.setBackgroundColor(lineColor);
                this.itemView.setOnClickListener((view) -> {
                    BoiteSingleton.getInstance().setCurrentBoite(boite);
                    goToBoiteDetail(boite.getId());
                });
                mNom.setText(boite.getNom());
                mHoraire.setText(boite.getHoraireOuverture());
                Picasso.get().load(boite.getUrlImage()).into(mLogo, new Callback() {
                    @Override
                    public void onSuccess() {
                    }

                    @Override
                    public void onError(Exception e) {
                        Log.d("BoiteAdapter", e.getMessage());
                    }
                });
            }
        }
    }
}
package com.barthelemot.kylian.projetboitedenuit;

import android.app.Activity;
import android.content.Intent;
import android.database.SQLException;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.barthelemot.kylian.projetboitedenuit.dataBase.DBManager;
import com.barthelemot.kylian.projetboitedenuit.singleton.UserSingleton;

public class MesinfosActivity extends AppCompatActivity {

    private DBManager mDBManager;

    private TextView mMail;
    private TextView mMdp;

    private Button maj;

    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mesinfos);
        mMail = (TextView) findViewById(R.id.mail);
        mMdp = (TextView) findViewById(R.id.password);
        maj = (Button) findViewById(R.id.maj);
        mMail.setText(UserSingleton.getInstance().getCurrentUser().getMail());
        mMdp.setText(UserSingleton.getInstance().getCurrentUser().getMdp());

        mDBManager = new DBManager(this);
        try{
            mDBManager.open();
        } catch (SQLException e) {
            e.printStackTrace();
            Log.d("Database",e.getLocalizedMessage());
        }

        mToolbar = (Toolbar) findViewById(R.id.toolBar);
        mToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_white));
        mToolbar.setNavigationOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                goToMainActivity();
            }
        });


        maj.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    goToMajActivity();
            }
        });
    }

    private void goToMainActivity() {
        Intent intent = new Intent (this,MainActivity.class);
        startActivity(intent);
    }

    public void goToMajActivity() { // Méthode pour envoyer vers l'activité mise a jour
        Intent intent = new Intent(this, MajActivity.class);
        startActivity(intent);
    }


}


package com.barthelemot.kylian.projetboitedenuit;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager2.widget.ViewPager2;

public class PagerBoiteActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.boite_pager);
        ViewPager2 viewPager = findViewById(R.id.pager);
        viewPager.setAdapter(new BoitePagerAdapter(this));
        viewPager.setCurrentItem((int)getIntent().getExtras().get("id")-1);
    }
}

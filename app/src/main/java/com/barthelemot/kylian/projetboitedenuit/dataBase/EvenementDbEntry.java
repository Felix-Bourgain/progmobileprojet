package com.barthelemot.kylian.projetboitedenuit.dataBase;

import android.provider.BaseColumns;

public abstract class EvenementDbEntry implements BaseColumns {
    public static final String TABLE_NAME = "evenement";
    public static final String COLUMN_NAME_ID = "idEvenement";
    public static final String COLUMN_NAME_NOM = "nomEvenement";
    public static final String COLUMN_NAME_DATE = "dateEvenement";
    public static final String COLUMN_NAME_DESCRIPTION = "descriptionEvenement";
    public static final String COLUMN_NAME_URLIMAGE = "urlImageEvenement";
    public static final String COLUMN_NAME_BOITE = "idBoite";
}
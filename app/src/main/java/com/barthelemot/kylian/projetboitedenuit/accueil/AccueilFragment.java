package com.barthelemot.kylian.projetboitedenuit.accueil;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.barthelemot.kylian.projetboitedenuit.PagerBoiteActivity;
import com.barthelemot.kylian.projetboitedenuit.R;
import com.barthelemot.kylian.projetboitedenuit.dataBase.EvenementDbEntry;
import com.barthelemot.kylian.projetboitedenuit.entity.Evenement;
import com.barthelemot.kylian.projetboitedenuit.singleton.BoiteSingleton;
import com.barthelemot.kylian.projetboitedenuit.singleton.EventSingleton;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class AccueilFragment extends Fragment {

    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_accueil, container, false);

        recyclerView = root.findViewById(R.id.listEvent);
        layoutManager = new LinearLayoutManager(this.getContext());
        recyclerView.setLayoutManager(layoutManager);
        adapter = new EventAdapter();
        recyclerView.setAdapter(adapter);

        return root;
    }

    public void goToBoiteDetail(int id) {
        Intent intent = new Intent(this.getContext(), PagerBoiteActivity.class);
        intent.putExtra("id", id);
        startActivity(intent);
    }

    public class EventAdapter extends RecyclerView.Adapter<EventAdapter.ViewHolder> {

        public EventAdapter(){
            super();
        }

        @NonNull
        @Override
        public EventAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.event_item_list, parent, false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(@NonNull EventAdapter.ViewHolder holder, int position) {
            if(position % 2 == 0) {
                holder.setEvent(EventSingleton.getInstance().getEvent(position), Color.rgb(2, 176, 159));
            }
            else {
                holder.setEvent(EventSingleton.getInstance().getEvent(position), Color.rgb(161,161,161));
            }
        }

        @Override
        public int getItemCount() {
            return EventSingleton.getInstance().getCountEvent();
        }


        public class ViewHolder extends RecyclerView.ViewHolder {
            private ImageView mLogo;
            private TextView mNom;
            private TextView mDate;
            private TextView mNomBoite;

            public ViewHolder(View v) {
                super(v);
                mLogo = (ImageView) v.findViewById(R.id.imageView_logoEvent);
                mNom = (TextView) v.findViewById(R.id.tv_eventNom);
                mDate = (TextView) v.findViewById(R.id.tv_eventDate);
                mNomBoite = (TextView) v.findViewById(R.id.tv_boiteNom);
            }

            public void setEvent(final Evenement event, int lineColor) {
                this.itemView.setBackgroundColor(lineColor);
                this.itemView.setOnClickListener((view) -> {
                    EventSingleton.getInstance().setCurrentEvent(event);
                    goToBoiteDetail(event.getBoite().getId());
                });
                mNom.setText(event.getNom());
                mDate.setText(event.getDate().format(DateTimeFormatter.ofPattern("d/MM/yyyy")));
                mNomBoite.setText(event.getBoite().getNom());
                Picasso.get().load(event.getUrlImage()).into(mLogo, new Callback() {
                    @Override
                    public void onSuccess() {
                    }
                    @Override
                    public void onError(Exception e) {
                        Log.d("EventAdapter", e.getMessage());
                    }
                });
            }
        }
    }
}
package com.barthelemot.kylian.projetboitedenuit;

import android.content.ContentValues;
import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.barthelemot.kylian.projetboitedenuit.dataBase.AppDbHelper;
import com.barthelemot.kylian.projetboitedenuit.dataBase.DBManager;
import com.barthelemot.kylian.projetboitedenuit.dataBase.UtilisateurDbEntry;
import com.barthelemot.kylian.projetboitedenuit.singleton.UserSingleton;

import java.util.ArrayList;

public class MajActivity extends AppCompatActivity {
    private DBManager mDBManager;
    private EditText mail;
    private EditText mdp;
    private EditText confirmMdp;
    private Button btnValider;

    private Toolbar mToolbar;

    private boolean verification;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maj);

        mDBManager = new DBManager(this);
        try{
            mDBManager.open();
        } catch (SQLException e) {
            e.printStackTrace();
            Log.d("Database",e.getLocalizedMessage());
        }


        UserSingleton.getInstance().setUserList((ArrayList) mDBManager.getAllUtilisateurs());

        mail = (EditText) findViewById(R.id.register_mail);
        mail.setText(UserSingleton.getInstance().getCurrentUser().getMail());
        mdp = (EditText) findViewById(R.id.register_mdp);
        mdp.setText(UserSingleton.getInstance().getCurrentUser().getMdp());
        confirmMdp = (EditText) findViewById(R.id.register_confirm_mdp);

        verification = true;

        mToolbar = (Toolbar) findViewById(R.id.toolBar);
        mToolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_arrow_back_white));
        mToolbar.setNavigationOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                goToMesinfosActivity();
            }
        });

        btnValider = (Button) findViewById(R.id.register_button);

        btnValider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                maj();
            }

        });
    }

    public void maj() {
        verification = true;

        if(mail.getText().toString().trim().equalsIgnoreCase("")){
            mail.setError("Entrer un mail valide");
            verification = false;
        }
        if(mdp.getText().toString().trim().equalsIgnoreCase("")){
            mdp.setError("Entrer un mot de passe");
            verification = false;
        }
        if(confirmMdp.getText().toString().trim().equalsIgnoreCase("")){
            confirmMdp.setError("Veuillez confirmez votre mot de passe");
            verification = false;
        }
        if (!confirmMdp.getText().toString().equals(mdp.getText().toString())) {
            confirmMdp.setError("Les mots de passes sont différents");
            verification = false;
        }

        if(verification == true){
            UserSingleton.getInstance().getCurrentUser().setMail(mail.getText().toString());
            UserSingleton.getInstance().getCurrentUser().setMdp(mdp.getText().toString());
            mDBManager.majUtilisateur(UserSingleton.getInstance().getCurrentUser());
            goToMesinfosActivity();
        }

    }

    public void goToMesinfosActivity() {
        Intent intent = new Intent(this, MesinfosActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onResume(){
        super.onResume();
        try{
            mDBManager.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mDBManager.close();
    }

}

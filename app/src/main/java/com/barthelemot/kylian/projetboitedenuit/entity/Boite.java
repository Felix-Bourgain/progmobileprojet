package com.barthelemot.kylian.projetboitedenuit.entity;

import android.util.Log;

public class Boite {

    private int id;
    private String nom;
    private String adresse;
    private String tel;
    private String description;
    private String urlImage;
    private int ageMoyen;
    private String horaireOuverture;
    private boolean ouvert; //Oui ou Non


    public Boite() {}

    public Boite(String nom, String adresse, String tel, String description, String urlImage, int ageMoyen, String horaireOuverture, boolean ouvert) {
        this.nom = nom;
        this.adresse = adresse;
        this.tel = tel;
        this.description = description;
        this.urlImage = urlImage;
        this.ageMoyen = ageMoyen;
        this.horaireOuverture = horaireOuverture;
        this.ouvert = ouvert;
        Log.d("TestAddBoiteConstructeur", this.isOuvert()+"");
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }

    public int getAgeMoyen() {
        return ageMoyen;
    }

    public void setAgeMoyen(int ageMoyen) {
        this.ageMoyen = ageMoyen;
    }

    public String getHoraireOuverture() {
        return horaireOuverture;
    }

    public void setHoraireOuverture(String horaireOuverture) {
        this.horaireOuverture = horaireOuverture;
    }

    public boolean isOuvert() {
        return ouvert;
    }

    public void setOuvert(boolean ouvert) {
        this.ouvert = ouvert;
    }
}

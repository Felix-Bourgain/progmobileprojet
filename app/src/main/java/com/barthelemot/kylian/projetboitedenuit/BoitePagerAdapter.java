package com.barthelemot.kylian.projetboitedenuit;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.viewpager2.adapter.FragmentStateAdapter;

import com.barthelemot.kylian.projetboitedenuit.listeboites.ListeboitesFragment;
import com.barthelemot.kylian.projetboitedenuit.singleton.BoiteSingleton;

public class BoitePagerAdapter extends FragmentStateAdapter {

    public BoitePagerAdapter(FragmentActivity fragmentActivity){
        super(fragmentActivity);
    }

    @Override
    public Fragment createFragment(int position) {
        return new BoiteFragment(BoiteSingleton.getInstance().getBoite(position));
    }

    @Override
    public int getItemCount() {
        return BoiteSingleton.getInstance().getCountBoite();
    }
}

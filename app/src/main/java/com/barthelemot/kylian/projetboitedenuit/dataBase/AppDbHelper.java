package com.barthelemot.kylian.projetboitedenuit.dataBase;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class AppDbHelper extends SQLiteOpenHelper {

    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "boite.db";

    private static final String TEXT_TYPE = " TEXT";
    private static final String INTEGER_TYPE = " INTEGER";
    private static final String DATE_TYPE = " DATE";
    private static final String COMMA_SEP = ",";

    public AppDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_DELETE_TABLE_EVENEMENT);
        db.execSQL(SQL_DELETE_TABLE_BOITE);
        db.execSQL(SQL_DELETE_TABLE_UTILISATEUR);
        db.execSQL(SQL_CREATE_TABLE_UTILISATEUR);
        db.execSQL(SQL_CREATE_TABLE_BOITE);
        db.execSQL(SQL_CREATE_TABLE_EVENEMENT);
        db.execSQL(SQL_INSERT_UTILISATEURS);
        db.execSQL(SQL_INSERT_BOITES);
        db.execSQL(SQL_INSERT_EVENEMENTS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL(SQL_DELETE_TABLE_EVENEMENT+SQL_DELETE_TABLE_BOITE+SQL_DELETE_TABLE_UTILISATEUR);
        onCreate(db);
    }

    /* ---------- UTILISATEUR ---------- */

    private static final String SQL_CREATE_TABLE_UTILISATEUR = "CREATE TABLE IF NOT EXISTS " + UtilisateurDbEntry.TABLE_NAME + " (" +
            UtilisateurDbEntry.COLUMN_NAME_ID + " INTEGER PRIMARY KEY" + COMMA_SEP +
            UtilisateurDbEntry.COLUMN_NAME_NOM + TEXT_TYPE + COMMA_SEP +
            UtilisateurDbEntry.COLUMN_NAME_PRENOM + TEXT_TYPE + COMMA_SEP +
            UtilisateurDbEntry.COLUMN_NAME_MAIL + TEXT_TYPE + COMMA_SEP +
            UtilisateurDbEntry.COLUMN_NAME_MDP + TEXT_TYPE + COMMA_SEP +
            UtilisateurDbEntry.COLUMN_NAME_ADMIN + INTEGER_TYPE + " );";

    private static final String SQL_DELETE_TABLE_UTILISATEUR = "DROP TABLE IF EXISTS " + UtilisateurDbEntry.TABLE_NAME +";";

    private static final String SQL_INSERT_UTILISATEURS = "INSERT INTO " + UtilisateurDbEntry.TABLE_NAME + "(" +
            UtilisateurDbEntry.COLUMN_NAME_ID + COMMA_SEP +
            UtilisateurDbEntry.COLUMN_NAME_NOM + COMMA_SEP +
            UtilisateurDbEntry.COLUMN_NAME_PRENOM + COMMA_SEP +
            UtilisateurDbEntry.COLUMN_NAME_MAIL + COMMA_SEP +
            UtilisateurDbEntry.COLUMN_NAME_MDP + COMMA_SEP +
            UtilisateurDbEntry.COLUMN_NAME_ADMIN + " ) VALUES (" +
            1 + COMMA_SEP + "'Barthelemot'" + COMMA_SEP + "'Kylian'" + COMMA_SEP + "'kylian.barthelemot@student.junia.com'" + COMMA_SEP + "'1234'" + COMMA_SEP + 1 + ")" + COMMA_SEP +
            "(" + 2 + COMMA_SEP + "'Bourgain'" + COMMA_SEP + "'Félix'" + COMMA_SEP + "'felix.bourgain@student.junia.com'" + COMMA_SEP + "'1234'" + COMMA_SEP + 1 + ")" + COMMA_SEP +
            "(" + 3 + COMMA_SEP + "'Dupont'" + COMMA_SEP + "'Jean'" + COMMA_SEP + "'jean.dupont@gmail.com'" + COMMA_SEP + "'1234'" + COMMA_SEP + 0 + ")" + COMMA_SEP +
            "(" + 4 + COMMA_SEP + "'Roulet'" + COMMA_SEP + "'Lisa'" + COMMA_SEP + "'lisa.roulet@student.junia.com'" + COMMA_SEP + "'1234'" + COMMA_SEP + 0 + ")" + COMMA_SEP +
            "(" + 5 + COMMA_SEP + "'Camboulives'" + COMMA_SEP + "'Julien'" + COMMA_SEP + "'julien.camboulives-bedecarasburu@student.junia.com'" + COMMA_SEP + "'1234'" + COMMA_SEP + 0 + ")" + COMMA_SEP +
            "(" + 6 + COMMA_SEP + "'Robyns'" + COMMA_SEP + "'Jonathan'" + COMMA_SEP + "'jonathan.robyns@student.junia.com'" + COMMA_SEP + "'1234'" + COMMA_SEP + 0 + ")" + COMMA_SEP +
            "(" + 7 + COMMA_SEP + "'Paturel'" + COMMA_SEP + "'Ronan'" + COMMA_SEP + "'ronan.paturel@student.junia.com'" + COMMA_SEP + "'1234'" + COMMA_SEP + 0 + ")"
            + ";";

    /* ---------- BOITE ---------- */

    private static final String SQL_CREATE_TABLE_BOITE = "CREATE TABLE IF NOT EXISTS " + BoiteDbEntry.TABLE_NAME + " (" +
            BoiteDbEntry.COLUMN_NAME_ID + " INTEGER PRIMARY KEY" + COMMA_SEP +
            BoiteDbEntry.COLUMN_NAME_NOM + TEXT_TYPE + COMMA_SEP +
            BoiteDbEntry.COLUMN_NAME_ADRESSE + TEXT_TYPE + COMMA_SEP +
            BoiteDbEntry.COLUMN_NAME_TEL + TEXT_TYPE + COMMA_SEP +
            BoiteDbEntry.COLUMN_NAME_DESCRIPTION + TEXT_TYPE + COMMA_SEP +
            BoiteDbEntry.COLUMN_NAME_URLIMAGE + TEXT_TYPE + COMMA_SEP +
            BoiteDbEntry.COLUMN_NAME_AGEMOYEN + INTEGER_TYPE + COMMA_SEP +
            BoiteDbEntry.COLUMN_NAME_HORAIREOUVERTURE + TEXT_TYPE + COMMA_SEP +
            BoiteDbEntry.COLUMN_NAME_OUVERT + INTEGER_TYPE  +" );";

    private static final String SQL_DELETE_TABLE_BOITE = "DROP TABLE IF EXISTS " + BoiteDbEntry.TABLE_NAME + ";";

    private static final String SQL_INSERT_BOITES = "INSERT INTO " + BoiteDbEntry.TABLE_NAME + "(" +
            BoiteDbEntry.COLUMN_NAME_ID + COMMA_SEP +
            BoiteDbEntry.COLUMN_NAME_NOM + COMMA_SEP +
            BoiteDbEntry.COLUMN_NAME_ADRESSE + COMMA_SEP +
            BoiteDbEntry.COLUMN_NAME_TEL + COMMA_SEP +
            BoiteDbEntry.COLUMN_NAME_DESCRIPTION + COMMA_SEP +
            BoiteDbEntry.COLUMN_NAME_URLIMAGE + COMMA_SEP +
            BoiteDbEntry.COLUMN_NAME_AGEMOYEN + COMMA_SEP +
            BoiteDbEntry.COLUMN_NAME_HORAIREOUVERTURE + COMMA_SEP +
            BoiteDbEntry.COLUMN_NAME_OUVERT + " ) VALUES (" +
            1 + COMMA_SEP + "'Le Smile'" + COMMA_SEP + "'3 Rue Ernest Deconynck Lille France'" + COMMA_SEP + "'0320570416'" + COMMA_SEP + "'Quel autre lieu dans tout Lille peut se vanter de pouvoir faire danser et faire s’amuser des gens ayant des goûts totalement opposés ? Le Smile Club Lille uniquement. Ce qui fait la réussite et le succès de ce lieu depuis toutes ces années est le fait que tout le monde peut trouver l’ambiance qui lui convient et qui lui permet de s’amuser comme il l’entend et comme il le veut.  Dès son entrée dans la discothèque, le client est immédiatement plongé dans une atmosphère festive et survoltée. Tout a été fait et pensé pour que vous n’ayez pas besoin de temps d’adaptation et que vous vous sentiez tout de suite à l’aise.'" + COMMA_SEP + "'https://www.lillelanuit.com/wp-content/uploads/cache/smile-club-lille_w450_q70_279e2d99f5e4153e59282808c068e7de.jpg?fbclid=IwAR2z--gnAV523GrUZpf20Rytl2gv6g-AnQodWwgTD3r518D1u71Aa8VWiu0'" + COMMA_SEP + 20 + COMMA_SEP + "'22h - 06h'" + COMMA_SEP + 0 + ")" + COMMA_SEP +
            "(" + 2 + COMMA_SEP + "'La Relève'" + COMMA_SEP + "'14 Rue Masséna Lille France'" + COMMA_SEP + "'0662662367'" + COMMA_SEP + "'18h, une petite sieste stratégique s’impose : je veux être en forme pour danser toute la nuit. Parce que dans la légendaire rue de la Soif, la Relève c’est sacré : ça ne se refuse jamais ! La techno et les lumières endiablées sont le combo parfait pour vous ensorceler dans ce sous-sol berlinois enfumé. 6h, « Boum Boum dans les oreilles ». Mes potes m’extirpent de la piste de dance. Mon égo de fêtard est touché mais c’est promis je me Relèverai ! '" + COMMA_SEP + "'https://www.lillelanuit.com/wp-content/uploads/cache/800_4433_releve_w450_q70_0d15d2710d897ca5c81778204a279e42.jpg?fbclid=IwAR0_EyTlu3rBcBNmtYlDXpd47cVSC4ikwGRmYyuaNmFKN83nuGO36f8mN70'" + COMMA_SEP + 25 + COMMA_SEP + "'23h - 06h'" + COMMA_SEP + 0 + ")" + COMMA_SEP +
            "(" + 3 + COMMA_SEP + "'O Corner'" + COMMA_SEP + "'21 Rue Masséna Lille France'" + COMMA_SEP + "'0613231352'" + COMMA_SEP + "'Beers, music and live sports'" + COMMA_SEP + "'https://scontent-cdg2-1.xx.fbcdn.net/v/t1.0-9/62214054_620693968448679_6818782700099338240_n.jpg?_nc_cat=108&ccb=1-3&_nc_sid=e3f864&_nc_ohc=c6ajZ3m6bCAAX8lWrhc&_nc_ht=scontent-cdg2-1.xx&oh=52355d78346f32379b8b2e58ea904918&oe=606DAE58'" + COMMA_SEP + 22 + COMMA_SEP + "'17h - 07h'" + COMMA_SEP + 1 + ")" + COMMA_SEP +
            "(" + 4 + COMMA_SEP + "'The Room'" + COMMA_SEP + "'53 Rue Léon Gambetta Lille France'" + COMMA_SEP + "'0650579001'" + COMMA_SEP + "'Le ROOM CLUB est le dernier né des clubs pictaviens puisqu’il a ouvert ses portes fin 2014. Il est situé au 37 boulevard du Grand Cerf à Poitiers, à deux pas de la gare TGV, dans un espace fait de beaux volumes s’articulant autour d’un dancefloor central devançant une scène modulable pouvant accueillir tous types de prestations, telles que concerts, spectacles ou dj set. Sa capacité totale est de 250 personnes, il est par ailleurs équipé d’un sound system D&B qui se place d’emblée parmi les meilleurs de France.\n" + "Musicalement, ses nuits électro laissent la part belle aux headliners français et internationaux mais le room club est également défricheur et sait confier ses platines à des artistes ou des labels en devenir. '" + COMMA_SEP + "'https://media.istockphoto.com/vectors/vector-handwritten-logo-letter-r-vector-id1008257372?k=6&m=1008257372&s=170667a&w=0&h=5zU-nayGudYqYej83Phf-jQbQbQyaKJ7CPNH2AjyHz4%3D&fbclid=IwAR1Rm1FmsGtkR_327nx6KKp3mUsgTnjotsflV1TnlG2F5d4h__oPOIgU4A8'" + COMMA_SEP + 30 + COMMA_SEP + "'00h - 06h'" + COMMA_SEP + 0 + ")"
            + ";";

    /* ---------- EVENEMENT ---------- */

    private static final String SQL_CREATE_TABLE_EVENEMENT = "CREATE TABLE IF NOT EXISTS " + EvenementDbEntry.TABLE_NAME + " (" +
            EvenementDbEntry.COLUMN_NAME_ID + " INTEGER PRIMARY KEY" + COMMA_SEP +
            EvenementDbEntry.COLUMN_NAME_NOM + TEXT_TYPE + COMMA_SEP +
            EvenementDbEntry.COLUMN_NAME_DATE + DATE_TYPE + COMMA_SEP +
            EvenementDbEntry.COLUMN_NAME_DESCRIPTION + TEXT_TYPE + COMMA_SEP +
            EvenementDbEntry.COLUMN_NAME_URLIMAGE + TEXT_TYPE + COMMA_SEP +
            EvenementDbEntry.COLUMN_NAME_BOITE + INTEGER_TYPE + " );";

    private static final String SQL_DELETE_TABLE_EVENEMENT = "DROP TABLE IF EXISTS " + EvenementDbEntry.TABLE_NAME + ";";

    private static final String SQL_INSERT_EVENEMENTS = "INSERT INTO " + EvenementDbEntry.TABLE_NAME + "(" +
            EvenementDbEntry.COLUMN_NAME_ID + COMMA_SEP +
            EvenementDbEntry.COLUMN_NAME_NOM + COMMA_SEP +
            EvenementDbEntry.COLUMN_NAME_DATE + COMMA_SEP +
            EvenementDbEntry.COLUMN_NAME_DESCRIPTION + COMMA_SEP +
            EvenementDbEntry.COLUMN_NAME_URLIMAGE + COMMA_SEP +
            EvenementDbEntry.COLUMN_NAME_BOITE + " ) VALUES (" +

            1 + COMMA_SEP + "'Soirée mousse'" + COMMA_SEP + "'27/02/2021'" + COMMA_SEP + "'Soirée de réouverture au Smile CLUB 10 euros l entrée'" + COMMA_SEP + "'https://www.lillelanuit.com/wp-content/uploads/cache/smile-club-lille_w450_q70_279e2d99f5e4153e59282808c068e7de.jpg?fbclid=IwAR2z--gnAV523GrUZpf20Rytl2gv6g-AnQodWwgTD3r518D1u71Aa8VWiu0'" + COMMA_SEP + 1 + ")" + COMMA_SEP +
            "(" + 2 + COMMA_SEP + "'DJ PEPS 1st concert'" + COMMA_SEP + "'27/02/2021'" + COMMA_SEP + "'Un DJ Lillois à l honneur'" + COMMA_SEP + "'https://www.lillelanuit.com/wp-content/uploads/cache/800_4433_releve_w450_q70_0d15d2710d897ca5c81778204a279e42.jpg?fbclid=IwAR0_EyTlu3rBcBNmtYlDXpd47cVSC4ikwGRmYyuaNmFKN83nuGO36f8mN70'" + COMMA_SEP + 2 + ")" + COMMA_SEP +
            "(" + 3 + COMMA_SEP + "'Soirée dansante'" + COMMA_SEP + "'02/03/2021'" + COMMA_SEP + "'Soirée tout publique / Entrée gratuite'" + COMMA_SEP + "'https://media.istockphoto.com/vectors/vector-handwritten-logo-letter-r-vector-id1008257372?k=6&m=1008257372&s=170667a&w=0&h=5zU-nayGudYqYej83Phf-jQbQbQyaKJ7CPNH2AjyHz4%3D&fbclid=IwAR1Rm1FmsGtkR_327nx6KKp3mUsgTnjotsflV1TnlG2F5d4h__oPOIgU4A8'" + COMMA_SEP + 4 + ")" + COMMA_SEP +
            "(" + 4 + COMMA_SEP + "'Evénement annulé'" + COMMA_SEP + "'05/03/2021'" + COMMA_SEP + "'Evénement annulé au Smile CLUB'" + COMMA_SEP + "'https://www.lillelanuit.com/wp-content/uploads/cache/smile-club-lille_w450_q70_279e2d99f5e4153e59282808c068e7de.jpg?fbclid=IwAR2z--gnAV523GrUZpf20Rytl2gv6g-AnQodWwgTD3r518D1u71Aa8VWiu0'" + COMMA_SEP + 1 + ")"
            + ";";
}

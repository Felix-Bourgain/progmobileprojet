package com.barthelemot.kylian.projetboitedenuit.entity;

import java.time.LocalDate;

public class Evenement {
    private int id;
    private String nom;
    private LocalDate date;
    private String description;
    private String urlImage;
    private Boite boite;

    public Evenement() {};

    public Evenement(String nom, LocalDate date, String description, String urlImage, Boite boite) {
        this.nom = nom;
        this.date = date;
        this.description = description;
        this.urlImage = urlImage;
        this.boite = boite;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }

    public Boite getBoite() {
        return boite;
    }

    public void setBoite(Boite boite) {
        this.boite = boite;
    }
}

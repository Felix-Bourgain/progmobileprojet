package com.barthelemot.kylian.projetboitedenuit.mesinfos;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.barthelemot.kylian.projetboitedenuit.R;

public class MesinfosFragment extends Fragment {

    private MesinfosViewModel mesinfosViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        mesinfosViewModel =
                new ViewModelProvider(this).get(MesinfosViewModel.class);
        View root = inflater.inflate(R.layout.fragment_mesinfos, container, false);
        final TextView textView = root.findViewById(R.id.text_mesinfos);
        mesinfosViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });
        return root;
    }
}
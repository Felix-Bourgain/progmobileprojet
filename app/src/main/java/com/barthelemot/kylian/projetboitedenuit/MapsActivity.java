package com.barthelemot.kylian.projetboitedenuit;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.barthelemot.kylian.projetboitedenuit.singleton.BoiteSingleton;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     *
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        LatLng smile = new LatLng(50.6320292,3.0553982);
        LatLng releve = new LatLng(50.6332982,3.0549429);
        LatLng corner = new LatLng(50.6331117,3.0551955);
        LatLng room = new LatLng(50.6284212,3.0520797);
        LatLng sydney = new LatLng(-34, 151);

        //Le Smile
        //La Relève
        //O Corner
        //The Room
        if (BoiteSingleton.getInstance().getCurrentBoite().getNom().equals("Le Smile")) {
            mMap.addMarker(new MarkerOptions()
                    .position(smile)
                    .title("Localisation du Smile"));
            //mMap.animateCamera(CameraUpdateFactory.zoomTo(15), 2000, null);
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(smile, 15));
        }

        if (BoiteSingleton.getInstance().getCurrentBoite().getNom().equals("La Relève")) {
            mMap.addMarker(new MarkerOptions()
                    .position(releve)
                    .title("Localisation de la Relève"));
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(releve, 15));
        }
        if (BoiteSingleton.getInstance().getCurrentBoite().getNom().equals("O Corner")) {
            mMap.addMarker(new MarkerOptions()
                    .position(corner)
                    .title("Localisation du Corner"));
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(corner, 15));
        }
        if (BoiteSingleton.getInstance().getCurrentBoite().getNom().equals("The Room")) {
            mMap.addMarker(new MarkerOptions()
                    .position(room)
                    .title("Localisation de The Room"));
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(room, 15));
        }
    }
}

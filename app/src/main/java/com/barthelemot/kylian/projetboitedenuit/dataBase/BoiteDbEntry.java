package com.barthelemot.kylian.projetboitedenuit.dataBase;

import android.provider.BaseColumns;

public abstract class BoiteDbEntry implements BaseColumns {
    public static final String TABLE_NAME = "boite";
    public static final String COLUMN_NAME_ID = "idBoite";
    public static final String COLUMN_NAME_NOM = "nomBoite";
    public static final String COLUMN_NAME_ADRESSE = "adresseBoite";
    public static final String COLUMN_NAME_TEL = "telBoite";
    public static final String COLUMN_NAME_DESCRIPTION = "descriptionBoite";
    public static final String COLUMN_NAME_URLIMAGE = "urlImageBoite";
    public static final String COLUMN_NAME_AGEMOYEN = "ageMoyenBoite";
    public static final String COLUMN_NAME_HORAIREOUVERTURE = "horaireOuverture";
    public static final String COLUMN_NAME_OUVERT = "ouvert";
}

package com.barthelemot.kylian.projetboitedenuit.mesinfos;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.barthelemot.kylian.projetboitedenuit.singleton.UserSingleton;

public class MesinfosViewModel extends ViewModel {

    private MutableLiveData<String> mText;
    private MutableLiveData<String> mail;
    private MutableLiveData<String> mdp;

    public MesinfosViewModel() {
        mText = new MutableLiveData<>();
        mail = new MutableLiveData<>();
        mdp = new MutableLiveData<>();
        mText.setValue("This is mesinfos fragment");
        mail.setValue(UserSingleton.getInstance().getCurrentUser().getMail());
        mdp.setValue(UserSingleton.getInstance().getCurrentUser().getMdp());

    }

    public LiveData<String> getText() {
        return mText;
    }
    public LiveData<String> getMail() {
        return mail;
    }
    public LiveData<String> getMdp() {
        return mdp;
    }

}
package com.barthelemot.kylian.projetboitedenuit.singleton;

import com.barthelemot.kylian.projetboitedenuit.entity.Boite;

import java.util.ArrayList;

public class BoiteSingleton {
    private static final BoiteSingleton instance = new BoiteSingleton();

    private ArrayList<Boite> boiteList = new ArrayList<>();
    private Boite currentBoite;

    public static BoiteSingleton getInstance(){
        return instance;
    }

    public void setBoiteList(ArrayList<Boite> _boiteList) {
        this.boiteList = _boiteList;
    }

    public ArrayList<Boite> getBoiteList(){
        return this.boiteList;
    }

    public Boite getBoite(int index) {
        return boiteList.get(index);
    }

    public int getCountBoite() {
        return boiteList.size();
    }

    public void setCurrentBoite(Boite currentBoite) {
        this.currentBoite = currentBoite;
    }

    public Boite getCurrentBoite() {
        return currentBoite;
    }
}

package com.barthelemot.kylian.projetboitedenuit.dataBase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.barthelemot.kylian.projetboitedenuit.entity.Boite;
import com.barthelemot.kylian.projetboitedenuit.entity.Evenement;
import com.barthelemot.kylian.projetboitedenuit.entity.Utilisateur;
import com.barthelemot.kylian.projetboitedenuit.singleton.BoiteSingleton;
import com.barthelemot.kylian.projetboitedenuit.singleton.UserSingleton;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class DBManager {

    private SQLiteDatabase database;
    private AppDbHelper dbHelper;

    public DBManager(Context context) {
        dbHelper = new AppDbHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void createDataBaseSchema(){
        dbHelper.onCreate(database);
    }

    public void close() {
        dbHelper.close();
    }

    /* ---------- UTILISATEUR ---------- */

    public void addUtilisateur(Utilisateur utilisateur){
        ContentValues values = new ContentValues();
        values.put(UtilisateurDbEntry.COLUMN_NAME_NOM, utilisateur.getNom());
        values.put(UtilisateurDbEntry.COLUMN_NAME_PRENOM, utilisateur.getPrenom());
        values.put(UtilisateurDbEntry.COLUMN_NAME_MAIL, utilisateur.getMail());
        values.put(UtilisateurDbEntry.COLUMN_NAME_MDP, utilisateur.getMdp());
        values.put(UtilisateurDbEntry.COLUMN_NAME_ADMIN, utilisateur.isAdmin());

        Log.d("dbManagerAdd", utilisateur.getId() + " : " + utilisateur.getMail());

        database.insertWithOnConflict(UtilisateurDbEntry.TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_REPLACE);
    }
    public void majUtilisateur(Utilisateur utilisateur) {
        ContentValues values = new ContentValues();
        values.put(UtilisateurDbEntry.COLUMN_NAME_ID, utilisateur.getId());
        values.put(UtilisateurDbEntry.COLUMN_NAME_NOM, utilisateur.getNom());
        values.put(UtilisateurDbEntry.COLUMN_NAME_PRENOM, utilisateur.getPrenom());
        values.put(UtilisateurDbEntry.COLUMN_NAME_MAIL, utilisateur.getMail());
        values.put(UtilisateurDbEntry.COLUMN_NAME_MDP, utilisateur.getMdp());
        if(utilisateur.isAdmin()){
            values.put(UtilisateurDbEntry.COLUMN_NAME_ADMIN, 1);
        }
        else{
            values.put(UtilisateurDbEntry.COLUMN_NAME_ADMIN, 0);
        }
        database.replace(UtilisateurDbEntry.TABLE_NAME,null,values);
    }

    public void addfAKEUtilisateur(){
        ContentValues values = new ContentValues();
        values.put(UtilisateurDbEntry.COLUMN_NAME_NOM, "Lefebvre");
        values.put(UtilisateurDbEntry.COLUMN_NAME_PRENOM, "Pierre");
        values.put(UtilisateurDbEntry.COLUMN_NAME_MAIL, "roro");
        values.put(UtilisateurDbEntry.COLUMN_NAME_MDP, "dkc");
        values.put(UtilisateurDbEntry.COLUMN_NAME_ADMIN, 0);

        database.insertWithOnConflict(UtilisateurDbEntry.TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_REPLACE);
    }


    private Utilisateur cursorToUtilisateur(Cursor cursor) {
        Utilisateur user = new Utilisateur();
        user.setId(cursor.getInt(cursor.getColumnIndex(UtilisateurDbEntry.COLUMN_NAME_ID)));
        user.setNom(cursor.getString(cursor.getColumnIndex(UtilisateurDbEntry.COLUMN_NAME_NOM)));
        user.setPrenom(cursor.getString(cursor.getColumnIndex(UtilisateurDbEntry.COLUMN_NAME_PRENOM)));
        user.setMail(cursor.getString(cursor.getColumnIndex(UtilisateurDbEntry.COLUMN_NAME_MAIL)));
        user.setMdp(cursor.getString(cursor.getColumnIndex(UtilisateurDbEntry.COLUMN_NAME_MDP)));
        int admin = cursor.getInt(cursor.getColumnIndex(UtilisateurDbEntry.COLUMN_NAME_ADMIN));
        if(admin == 1){
            user.setAdmin(true);
        }
        else{
            user.setAdmin(false);
        }
        return user;
    }

    public List<Utilisateur> getAllUtilisateurs() {
        List<Utilisateur> userList = new ArrayList<>();

        Cursor cursor = database.rawQuery("Select * from " + UtilisateurDbEntry.TABLE_NAME, null);
        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
            Utilisateur user = cursorToUtilisateur(cursor);
            userList.add(user);
            cursor.moveToNext();
        }
        cursor.close();
        return userList;
    }

    /* ---------- BOITE ---------- */


    public void addBoite (Boite boite) {
        ContentValues values = new ContentValues();
        values.put(BoiteDbEntry.COLUMN_NAME_NOM, boite.getNom());
        values.put(BoiteDbEntry.COLUMN_NAME_ADRESSE, boite.getAdresse());
        values.put(BoiteDbEntry.COLUMN_NAME_TEL, boite.getTel());
        values.put(BoiteDbEntry.COLUMN_NAME_DESCRIPTION, boite.getDescription());
        values.put(BoiteDbEntry.COLUMN_NAME_URLIMAGE, boite.getUrlImage());
        values.put(BoiteDbEntry.COLUMN_NAME_AGEMOYEN, boite.getAgeMoyen());
        values.put(BoiteDbEntry.COLUMN_NAME_HORAIREOUVERTURE, boite.getHoraireOuverture());
        Log.d("TestAddBoiteManager", boite.isOuvert()+"");
        if(boite.isOuvert()){
            values.put(BoiteDbEntry.COLUMN_NAME_OUVERT, 1);
        }
        else{
            values.put(BoiteDbEntry.COLUMN_NAME_OUVERT, 0);
        }
        database.insertWithOnConflict(BoiteDbEntry.TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_REPLACE);
    }

    private Boite cursorToBoite(Cursor cursor) {
        Boite boite = new Boite();
        boite.setId(cursor.getInt(cursor.getColumnIndex(BoiteDbEntry.COLUMN_NAME_ID)));
        boite.setNom(cursor.getString(cursor.getColumnIndex(BoiteDbEntry.COLUMN_NAME_NOM)));
        boite.setAdresse(cursor.getString(cursor.getColumnIndex(BoiteDbEntry.COLUMN_NAME_ADRESSE)));
        boite.setTel(cursor.getString(cursor.getColumnIndex(BoiteDbEntry.COLUMN_NAME_TEL)));
        boite.setDescription(cursor.getString(cursor.getColumnIndex(BoiteDbEntry.COLUMN_NAME_DESCRIPTION)));
        boite.setUrlImage(cursor.getString(cursor.getColumnIndex(BoiteDbEntry.COLUMN_NAME_URLIMAGE)));
        boite.setAgeMoyen(cursor.getInt(cursor.getColumnIndex(BoiteDbEntry.COLUMN_NAME_AGEMOYEN)));
        boite.setHoraireOuverture(cursor.getString(cursor.getColumnIndex(BoiteDbEntry.COLUMN_NAME_HORAIREOUVERTURE)));
        boite.setOuvert(Boolean.valueOf(String.valueOf(cursor.getInt(cursor.getColumnIndex(BoiteDbEntry.COLUMN_NAME_OUVERT)))));
        int ouvert = cursor.getInt(cursor.getColumnIndex(BoiteDbEntry.COLUMN_NAME_OUVERT));
        if(ouvert == 1){
            boite.setOuvert(true);
        }
        else{
            boite.setOuvert(false);
        }
        return boite;
    }

    public List<Boite> getAllBoite() {
        List<Boite> boiteList = new ArrayList<>();

        Cursor cursor = database.rawQuery("Select * from " + BoiteDbEntry.TABLE_NAME, null);
        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
            Boite boite = cursorToBoite(cursor);
            boiteList.add(boite);
            cursor.moveToNext();
        }
        cursor.close();
        return boiteList;
    }

    /* ---------- EVENEMENT ---------- */

    public void addEvenement (Evenement evenement) {
        ContentValues values = new ContentValues();
        values.put(EvenementDbEntry.COLUMN_NAME_NOM, evenement.getNom());
        values.put(EvenementDbEntry.COLUMN_NAME_DATE, evenement.getDate().toString());
        values.put(EvenementDbEntry.COLUMN_NAME_DESCRIPTION, evenement.getDescription());
        values.put(EvenementDbEntry.COLUMN_NAME_URLIMAGE, evenement.getUrlImage());
        values.put(EvenementDbEntry.COLUMN_NAME_BOITE, evenement.getBoite().getId());

        database.insertWithOnConflict(EvenementDbEntry.TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_REPLACE);
    }

    private Evenement cursorToEvenement(Cursor cursor) {
        Evenement evenement = new Evenement();
        evenement.setId(cursor.getInt(cursor.getColumnIndex(EvenementDbEntry.COLUMN_NAME_ID)));
        evenement.setNom(cursor.getString(cursor.getColumnIndex(EvenementDbEntry.COLUMN_NAME_NOM)));
        evenement.setDate(LocalDate.parse(cursor.getString(cursor.getColumnIndex(EvenementDbEntry.COLUMN_NAME_DATE)), DateTimeFormatter.ofPattern("d/MM/yyyy")));
        evenement.setDescription(cursor.getString(cursor.getColumnIndex(EvenementDbEntry.COLUMN_NAME_DESCRIPTION)));
        evenement.setUrlImage(cursor.getString(cursor.getColumnIndex(EvenementDbEntry.COLUMN_NAME_URLIMAGE)));
        evenement.setBoite(BoiteSingleton.getInstance().getBoite(cursor.getInt(cursor.getColumnIndex(EvenementDbEntry.COLUMN_NAME_BOITE))-1));
        return evenement;
    }

    public List<Evenement> getAllEvenement() {
        List<Evenement> evenementList = new ArrayList<>();

        Cursor cursor = database.rawQuery("Select * from " + EvenementDbEntry.TABLE_NAME, null);
        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
            Evenement evenement = cursorToEvenement(cursor);
            evenementList.add(evenement);
            cursor.moveToNext();
        }
        cursor.close();
        return evenementList;
    }
}

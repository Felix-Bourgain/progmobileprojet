package com.barthelemot.kylian.projetboitedenuit.singleton;

import com.barthelemot.kylian.projetboitedenuit.entity.Boite;
import com.barthelemot.kylian.projetboitedenuit.entity.Evenement;

import java.util.ArrayList;

public class EventSingleton {
    private static final EventSingleton instance = new EventSingleton();

    private ArrayList<Evenement> EventList = new ArrayList<>();
    private Evenement currentEvent;

    public static EventSingleton getInstance(){
        return instance;
    }

    public void setEventList(ArrayList<Evenement> _EventList) {
        this.EventList = _EventList;
    }

    public ArrayList<Evenement> getEventList(){
        return this.EventList;
    }

    public Evenement getEvent(int index) {
        return EventList.get(index);
    }

    public int getCountEvent() {
        return EventList.size();
    }

    public void setCurrentEvent(Evenement currentEvent) {
        this.currentEvent = currentEvent;
    }

    public Evenement getCurrentEvent() {
        return currentEvent;
    }
}

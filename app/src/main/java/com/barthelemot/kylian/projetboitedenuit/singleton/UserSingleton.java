package com.barthelemot.kylian.projetboitedenuit.singleton;

import com.barthelemot.kylian.projetboitedenuit.entity.Utilisateur;

import java.util.ArrayList;

public class UserSingleton {
    private static final UserSingleton instance = new UserSingleton();

    private ArrayList<Utilisateur> userList = new ArrayList<>();
    private Utilisateur currentUser;

    public static UserSingleton getInstance(){
        return instance;
    }

    public void setUserList(ArrayList<Utilisateur> _userList) {
        this.userList = _userList;
    }

    public ArrayList<Utilisateur> getUserList(){
        return this.userList;
    }

    public Utilisateur getUtilisateur(int index) {
        return userList.get(index);
    }

    public int getCountUser() {
        return userList.size();
    }

    public void setCurrentUser(Utilisateur currentUser) {
        this.currentUser = currentUser;
    }

    public Utilisateur getCurrentUser() {
        return currentUser;
    }
}

package com.barthelemot.kylian.projetboitedenuit;

import android.content.Intent;
import android.database.SQLException;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.barthelemot.kylian.projetboitedenuit.dataBase.DBManager;
import com.barthelemot.kylian.projetboitedenuit.entity.Utilisateur;
import com.barthelemot.kylian.projetboitedenuit.singleton.UserSingleton;

import java.util.ArrayList;

public class RegisterActivity extends AppCompatActivity {

    private DBManager mDBManager;

    private EditText nom;
    private EditText prenom;
    private EditText mail;
    private EditText mdp;
    private EditText confirmMdp;
    private Button btnValider;

    private boolean verification;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mDBManager = new DBManager(this);
        try{
            mDBManager.open();
        } catch (SQLException e) {
            e.printStackTrace();
            Log.d("Database",e.getLocalizedMessage());
        }

        //mDBManager.addfAKEUtilisateur();

        UserSingleton.getInstance().setUserList((ArrayList) mDBManager.getAllUtilisateurs());

        nom = (EditText) findViewById(R.id.editText_nom);
        prenom = (EditText) findViewById(R.id.editText_prenom);
        mail = (EditText) findViewById(R.id.register_mail);
        mdp = (EditText) findViewById(R.id.editText_mdp);
        confirmMdp = (EditText) findViewById(R.id.register_confirm_mdp);

        verification = true;

        btnValider = (Button) findViewById(R.id.btn_register);

        btnValider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inscription();
            }

        });
    }

    public void inscription() {
        verification = true;
        if(nom.getText().toString().trim().equalsIgnoreCase("")){
            nom.setError("Entrer votre nom");
            verification = false;
        }
        if(prenom.getText().toString().trim().equalsIgnoreCase("")){
            prenom.setError("Entrer votre prenom");
            verification = false;
        }
        if(mail.getText().toString().trim().equalsIgnoreCase("")){
            mail.setError("Entrer un mail valide");
            verification = false;
        }
        if(mdp.getText().toString().trim().equalsIgnoreCase("")){
            mdp.setError("Entrer un mot de passe");
            verification = false;
        }
        if(confirmMdp.getText().toString().trim().equalsIgnoreCase("")){
            confirmMdp.setError("Entrer un mot de passe");
            verification = false;
        }
        if (!confirmMdp.getText().toString().equals(mdp.getText().toString())) {
            confirmMdp.setError("Les mots de passes sont différents");
            verification = false;
        }
        for (Utilisateur user : UserSingleton.getInstance().getUserList()) {
            if(mail.getText().toString().equals(user.getMail())){
                mail.setError("Cette adresse mail existe déjà");
                verification = false;
            }
        }
        if(verification == true){
            Utilisateur newUser = new Utilisateur(nom.getText().toString(), prenom.getText().toString(),mail.getText().toString(),mdp.getText().toString(), false);
            Log.d("RegisterAdd", newUser.getId() + " : " + newUser.getMail());
            mDBManager.addUtilisateur(newUser);
            goToHomeActivity();
        }
    }

    public void goToHomeActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onResume(){
        super.onResume();
        try{
            mDBManager.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mDBManager.close();
    }
}

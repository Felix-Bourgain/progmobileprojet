package com.barthelemot.kylian.projetboitedenuit;

import android.content.Intent;
import android.database.SQLException;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.barthelemot.kylian.projetboitedenuit.dataBase.DBManager;
import com.barthelemot.kylian.projetboitedenuit.entity.Boite;
import com.barthelemot.kylian.projetboitedenuit.entity.Evenement;
import com.barthelemot.kylian.projetboitedenuit.singleton.BoiteSingleton;
import com.barthelemot.kylian.projetboitedenuit.singleton.EventSingleton;

import java.time.LocalDate;
import java.util.ArrayList;

public class AjoutEventActivity extends AppCompatActivity {

    private DBManager mDBManager;

    private EditText mNomEvent;
    private DatePicker mDateEvent;
    private LocalDate date;
    private Spinner mListBoite;
    private TextView mTvDate;

    private ArrayAdapter spinnerAdapter;
    private ArrayList<String> nameListBoite;

    private Button btnValider;

    private boolean verification;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ajoutboite);

        mDBManager = new DBManager(this);
        try{
            mDBManager.open();
        } catch (SQLException e) {
            e.printStackTrace();
            Log.d("Database",e.getLocalizedMessage());
        }

        BoiteSingleton.getInstance().setBoiteList((ArrayList) mDBManager.getAllBoite());
        EventSingleton.getInstance().setEventList((ArrayList) mDBManager.getAllEvenement());

        mTvDate = (TextView) findViewById(R.id.tv_date);
        mNomEvent = (EditText) findViewById(R.id.editText_nomEvent);
        mDateEvent = (DatePicker) findViewById(R.id.datePicker);

        mListBoite = (Spinner) findViewById(R.id.spinnerListesBoites);
        nameListBoite = new ArrayList<String>();
        for(int i = 0; i < BoiteSingleton.getInstance().getCountBoite(); i++){
            nameListBoite.add(BoiteSingleton.getInstance().getBoiteList().get(i).getNom());
        }
        spinnerAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, nameListBoite);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mListBoite.setAdapter(spinnerAdapter);

        verification = true;

        btnValider = (Button) findViewById(R.id.btn_register);
        btnValider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enregistrer();
            }
        });
    }

    public void enregistrer() {
        verification = true;
        if(mNomEvent.getText().toString().trim().equalsIgnoreCase("")){
            mNomEvent.setError("Entrer un nom pour l'évènement");
            verification = false;
        }
        date = LocalDate.of(mDateEvent.getYear(), mDateEvent.getMonth(), mDateEvent.getDayOfMonth());
        if(date.isBefore(LocalDate.now()) || date.isEqual(LocalDate.now())){
            mTvDate.setError("Saisir une date supérieur à celle d'aujourd'hui");
            verification = false;
        }

        if(verification == true){
            String nomBoite = mListBoite.getSelectedItem().toString();
            Boite selectedBoite = new Boite();
            for(int i = 0; i < BoiteSingleton.getInstance().getCountBoite(); i++) {
                if(nomBoite.equals(BoiteSingleton.getInstance().getBoiteList().get(i).getNom())){
                   selectedBoite =  BoiteSingleton.getInstance().getBoiteList().get(i);
                }
            }
            Evenement newEvent = new Evenement(mNomEvent.getText().toString(), date, "", selectedBoite.getUrlImage(), selectedBoite);
            mDBManager.addEvenement(newEvent);
            goToHomeActivity();
        }
    }

    public void goToHomeActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onResume(){
        super.onResume();
        try{
            mDBManager.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mDBManager.close();
    }
}

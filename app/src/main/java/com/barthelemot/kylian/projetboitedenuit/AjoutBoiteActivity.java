package com.barthelemot.kylian.projetboitedenuit;

import android.content.Intent;
import android.database.SQLException;
import android.graphics.Color;
import android.os.Bundle;
import android.telephony.PhoneNumberFormattingTextWatcher;
import android.text.InputFilter;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Switch;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.barthelemot.kylian.projetboitedenuit.dataBase.DBManager;
import com.barthelemot.kylian.projetboitedenuit.entity.Boite;
import com.barthelemot.kylian.projetboitedenuit.entity.Utilisateur;
import com.barthelemot.kylian.projetboitedenuit.singleton.BoiteSingleton;
import com.barthelemot.kylian.projetboitedenuit.singleton.UserSingleton;
import com.barthelemot.kylian.projetboitedenuit.tool.InputFilterMinMax;

import java.util.ArrayList;


public class AjoutBoiteActivity extends AppCompatActivity {

    private DBManager mDBManager;

    private EditText mNomBoite;
    private EditText mAdresseBoite;
    private EditText mPhoneNumber;
    private EditText mDescription;
    private NumberPicker mAgeMoyen;
    private EditText mHoraireOuverture;
    private EditText mHoraireFermeture;
    private Switch mOuverture; //Si ouvert ou non
    private EditText mUrlImage;

    private Button btnValider;

    private boolean verification;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ajoutboite);

        mDBManager = new DBManager(this);
        try{
            mDBManager.open();
        } catch (SQLException e) {
            e.printStackTrace();
            Log.d("Database",e.getLocalizedMessage());
        }

        //mDBManager.addfAKEUtilisateur();
        BoiteSingleton.getInstance().setBoiteList((ArrayList) mDBManager.getAllBoite());

        mNomBoite = (EditText) findViewById(R.id.editText_nomBoite);
        mAdresseBoite = (EditText) findViewById(R.id.editText_adresseBoite);
        mPhoneNumber = (EditText) findViewById(R.id.editText_phoneNumber);
        mDescription = (EditText) findViewById(R.id.editText_description);
        mAgeMoyen = (NumberPicker) findViewById(R.id.numPicker_ageMoyen);
        mAgeMoyen.setMinValue(18);
        mAgeMoyen.setMaxValue(80);
        mHoraireOuverture = (EditText) findViewById(R.id.editText_heureOuverture);
        mHoraireOuverture.setFilters(new InputFilter[]{new InputFilterMinMax("00","23")});
        mHoraireFermeture = (EditText) findViewById(R.id.editText_heureFermeture);
        mHoraireFermeture.setFilters(new InputFilter[]{new InputFilterMinMax("00","23")});
        mOuverture = (Switch) findViewById(R.id.switch_Ouvert);
        mUrlImage = (EditText) findViewById(R.id.editText_urlLogo);

        verification = true;

        btnValider = (Button) findViewById(R.id.btn_register);
        btnValider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enregistrer();
            }
        });
    }

    public void enregistrer() {
        verification = true;
        if(mNomBoite.getText().toString().trim().equalsIgnoreCase("")){
            mNomBoite.setError("Entrer un nom de boite");
            verification = false;
        }
        for (Boite boite : BoiteSingleton.getInstance().getBoiteList()) {
            if(mNomBoite.getText().toString().equals(boite.getNom())){
                mNomBoite.setError("Ce nom est déjà pris");
                verification = false;
            }
        }
        if(mAdresseBoite.getText().toString().trim().equalsIgnoreCase("")){
            mAdresseBoite.setError("Entrer une adresse");
            verification = false;
        }
        if(mPhoneNumber.getText().toString().trim().equalsIgnoreCase("")){
            mPhoneNumber.setError("Entrer un numéro de téléphone");
            verification = false;
        }
        if(mDescription.getText().toString().trim().equalsIgnoreCase("")){
            mDescription.setError("Entrer une description");
            verification = false;
        }
        if(mHoraireOuverture.getText().toString().trim().equalsIgnoreCase("")){
            mHoraireOuverture.setError("Entrer une horaire d'ouverture");
            verification = false;
        }
        if(mHoraireFermeture.getText().toString().trim().equalsIgnoreCase("")){
            mHoraireFermeture.setError("Entrer une horaire de fermeture");
            verification = false;
        }
        if(mUrlImage.getText().toString().trim().equalsIgnoreCase("")){
            mUrlImage.setError("Entrer une URL pour votre image");
            verification = false;
        }
        if(verification == true){
            Boite newBoite = new Boite(mNomBoite.getText().toString(), mAdresseBoite.getText().toString(), mPhoneNumber.getText().toString(), mDescription.getText().toString(), mUrlImage.getText().toString(), mAgeMoyen.getValue(), (mHoraireOuverture.getText().toString() + "h - " + mHoraireFermeture.getText().toString()+"h"), mOuverture.isChecked());
            //Log.d("BoiteAdd", newBoite.getId() + " : " + newBoite.getNom());
            //Log.d("BoiteAddInfo", mNomBoite.getText().toString() + " " + mAdresseBoite.getText().toString() + " " + mPhoneNumber.getText().toString() + " " + mDescription.getText().toString() + " " + mUrlImage.getText().toString() + " " + mAgeMoyen.getValue() + " " + (mHoraireOuverture.getText().toString() + " - " + mHoraireFermeture.getText().toString()) + " " + mOuverture.isChecked());
            Log.d("TestAddBoiteActivity", newBoite.isOuvert()+"");
            mDBManager.addBoite(newBoite);
            goToHomeActivity();
        }
    }

    public void goToHomeActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onResume(){
        super.onResume();
        try{
            mDBManager.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mDBManager.close();
    }

}
